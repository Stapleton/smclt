// To disable a certain axe from being able to harvest trees, add an exclamation mark (!) in front of the line,
minecraft:wooden_axe,
minecraft:stone_axe,
minecraft:golden_axe,
minecraft:iron_axe,
minecraft:diamond_axe,
minecraft:netherite_axe,
create_dd:forest_ravager,
create_dd:deforester_saw,
modern_industrialization:diesel_chainsaw,
mythicmetals:adamantite_axe,
mythicmetals:aquarium_axe,
mythicmetals:banglum_axe,
mythicmetals:bronze_axe,
mythicmetals:carmot_axe,
mythicmetals:celestium_axe,
mythicmetals:copper_axe,
mythicmetals:durasteel_axe,
mythicmetals:hallowed_axe,
mythicmetals:kyber_axe,
mythicmetals:legendary_banglum_axe,
mythicmetals:metallurgium_axe,
mythicmetals:mythril_axe,
mythicmetals:orichalcum_axe,
mythicmetals:osmium_axe,
mythicmetals:palladium_axe,
mythicmetals:prometheum_axe,
mythicmetals:quadrillum_axe,
mythicmetals:runite_axe,
mythicmetals:star_platinum_axe,
mythicmetals:steel_axe,
mythicmetals:stormyx_axe,
mythicmetals:tidesinger_axe,
mythicupgrades:citrine_axe,
mythicupgrades:peridot_axe,
mythicupgrades:zircon_axe,
mythicupgrades:ruby_axe,
mythicupgrades:sapphire_axe,
mythicupgrades:topaz_axe,
mythicupgrades:ametrine_axe,
mythicupgrades:jade_axe,
spectrum:multitool,
spectrum:bedrock_axe,
spectrum:malachite_workstaff,
spectrum:glass_crest_workstaff,
ae2:certus_quartz_axe,
ae2:nether_quartz_axe,
ae2:fluix_axe,
