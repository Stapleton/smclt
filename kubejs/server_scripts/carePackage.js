const ModComponents = Java.loadClass("io.github.apace100.origins.registry.ModComponents");
const OriginLayers = Java.loadClass("io.github.apace100.origins.origin.OriginLayers");

const Diet = {
    Default: "32x farmersdelight:stuffed_pumpkin",
    Vegetarian: "32x garnished:garnished_meal",
    Carnivore: "32x createfood:cheeseburger_bacon",
    SugarAddict: "32x minecraft:honey_bottle",
    Raw: "64x garnished:raw_polar_bear_meat",
    Pescatarian: "32x farmersdelight:grilled_salmon",
    None: "8x spectrum:triple_meat_pot_pie",
    Custom: function (itemCount, itemID) {
        return `${itemCount}x ${itemID}`;
    },
};

const Packages = {
    CommonItems: ["4x origins:orb_of_origin", "16x minecraft:torch", "16x minecraft:honey_bottle"],
    "Alien Axolotl": {
        food: {
            diet: Diet.Default,
            hungryboi: false,
        },
        weapon: [],
        tool: [],
        gear: [],
        extras: [[]],
    },
    Anomaly: {
        food: {
            diet: Diet.Default,
            hungryboi: false,
        },
        weapon: [],
        tool: [],
        gear: [],
        extras: [[]],
    },
    Arachnid: {
        food: {
            diet: Diet.Carnivore,
            hungryboi: false,
        },
        weapon: [],
        tool: [],
        gear: [],
        extras: [[]],
    },
    "Artificial Construct": {
        food: {
            diet: Diet.None,
            hungryboi: false,
        },
        weapon: [],
        tool: [],
        gear: [],
        extras: ["16x minecraft:coal"],
    },
    Avian: {
        food: {
            diet: Diet.Vegetarian,
            hungryboi: false,
        },
        weapon: [],
        tool: [],
        gear: [],
        extras: [[]],
    },
    Automaton: {
        food: {
            diet: Diet.None,
            hungryboi: false,
        },
        weapon: [],
        tool: [],
        gear: [],
        extras: ["16x minecraft:coal", Item.of("minecraft:potion", 16, '{Potion:"minecraft:water"}')],
    },
    Beaver: {
        food: {
            diet: Diet.Default,
            hungryboi: true,
        },
        weapon: [],
        tool: [],
        gear: [],
        extras: [[]],
    },
    Bedrockean: {
        food: {
            diet: Diet.None,
            hungryboi: false,
        },
        weapon: [],
        tool: [],
        gear: [],
        extras: [[]],
    },
    Binturong: {
        food: {
            diet: Diet.Default,
            hungryboi: false,
        },
        weapon: [],
        tool: [],
        gear: [],
        extras: ["1x minecraft:red_dye"],
    },
    Birb: {
        food: {
            diet: Diet.Custom(64, "farmersdelight:tomato_seeds"),
            hungryboi: false,
        },
        weapon: [],
        tool: [],
        gear: [],
        extras: [[]],
    },
    Blazeborn: {
        food: {
            diet: Diet.Default,
            hungryboi: false,
        },
        weapon: [],
        tool: [],
        gear: [],
        extras: [[]],
    },
    Blazian: {
        food: {
            diet: Diet.Default,
            hungryboi: false,
        },
        weapon: [],
        tool: [],
        gear: [],
        extras: [[]],
    },
    Blob: {
        food: {
            diet: Diet.None,
            hungryboi: false,
        },
        weapon: [],
        tool: [],
        gear: [],
        extras: [[]],
    },
    Boarling: {
        food: {
            diet: Diet.Default,
            hungryboi: false,
        },
        weapon: [],
        tool: [],
        gear: [],
        extras: [[]],
    },
    Broodmother: {
        food: {
            diet: Diet.Carnivore,
            hungryboi: false,
        },
        weapon: [],
        tool: [],
        gear: [],
        extras: [[]],
    },
    "Calamitous Rogue": {
        food: {
            diet: Diet.Default,
            hungryboi: false,
        },
        weapon: [],
        tool: [],
        gear: [],
        extras: [[]],
    },
    "Candy-Person": {
        food: {
            diet: Diet.SugarAddict,
            hungryboi: false,
        },
        weapon: [],
        tool: [],
        gear: [],
        extras: [[]],
    },
    "Child Of Cthulhu": {
        food: {
            diet: Diet.Default,
            hungryboi: true,
        },
        weapon: [],
        tool: [],
        gear: [],
        extras: [[]],
    },
    Chimaera: {
        food: {
            diet: Diet.None,
            hungryboi: false,
        },
        weapon: [],
        tool: [],
        gear: [],
        extras: [[]],
    },
    Cobra: {
        food: {
            diet: Diet.Carnivore,
            hungryboi: false,
        },
        weapon: [],
        tool: [],
        gear: [],
        extras: [[]],
    },
    "Copper Golem": {
        food: {
            diet: Diet.Custom(8, "minecraft:copper_ingot"),
            hungryboi: false,
        },
        weapon: [],
        tool: [],
        gear: [],
        extras: ["8x minecraft:honeycomb"],
    },
    "Corrupted Wither": {
        food: {
            diet: Diet.Default,
            hungryboi: true,
        },
        weapon: [],
        tool: [],
        gear: [],
        extras: [[]],
    },
    Craftsman: {
        food: {
            diet: Diet.Default,
            hungryboi: false,
        },
        weapon: [],
        tool: [],
        gear: [],
        extras: [[]],
    },
    "Dark Mage": {
        food: {
            diet: Diet.Default,
            hungryboi: false,
        },
        weapon: [],
        tool: [],
        gear: [],
        extras: [[]],
    },
    Deathsworn: {
        food: {
            diet: Diet.Default,
            hungryboi: false,
        },
        weapon: [],
        tool: [],
        gear: [],
        extras: [[]],
    },
    "Demi-God": {
        food: {
            diet: Diet.Raw,
            hungryboi: false,
        },
        weapon: [],
        tool: [],
        gear: [],
        extras: [[]],
    },
    Deranged: {
        food: {
            diet: Diet.Default,
            hungryboi: false,
        },
        weapon: [],
        tool: [],
        gear: [],
        extras: [[]],
    },
    "Devine Architect": {
        food: {
            diet: Diet.Default,
            hungryboi: false,
        },
        weapon: [],
        tool: [],
        gear: [],
        extras: [[]],
    },
    Dolphin: {
        food: {
            diet: Diet.Pescatarian,
            hungryboi: false,
        },
        weapon: [],
        tool: [],
        gear: [],
        extras: [[]],
    },
    Drakonwither: {
        food: {
            diet: Diet.Carnivore,
            hungryboi: false,
        },
        weapon: [],
        tool: [],
        gear: [],
        extras: [[]],
    },
    Dullahan: {
        food: {
            diet: Diet.Default,
            hungryboi: false,
        },
        weapon: [],
        tool: [],
        gear: [],
        extras: [[]],
    },
    "Earth Spirit": {
        food: {
            diet: Diet.Default,
            hungryboi: false,
        },
        weapon: [],
        tool: [],
        gear: [],
        extras: [[]],
    },
    "Ebon-Wing": {
        food: {
            diet: Diet.Carnivore,
            hungryboi: true,
        },
        weapon: [],
        tool: [],
        gear: [],
        extras: [[]],
    },
    "Emblazing Warrior": {
        food: {
            diet: Diet.Default,
            hungryboi: true,
        },
        weapon: [],
        tool: [],
        gear: [],
        extras: [[]],
    },
    Enderian: {
        food: {
            diet: Diet.Default,
            hungryboi: false,
        },
        weapon: [],
        tool: [],
        gear: [],
        extras: [[]],
    },
    "End Mage": {
        food: {
            diet: Diet.Default,
            hungryboi: false,
        },
        weapon: [],
        tool: [],
        gear: [],
        extras: [[]],
    },
    Enigma: {
        food: {
            diet: Diet.Default,
            hungryboi: false,
        },
        weapon: [],
        tool: [],
        gear: [],
        extras: [[]],
    },
    Elytrian: {
        food: {
            diet: Diet.Default,
            hungryboi: false,
        },
        weapon: [],
        tool: [],
        gear: [],
        extras: [[]],
    },
    "Fallen Angel": {
        food: {
            diet: Diet.Carnivore,
            hungryboi: false,
        },
        weapon: [],
        tool: [],
        gear: [],
        extras: [[]],
    },
    Feline: {
        food: {
            diet: Diet.Default,
            hungryboi: false,
        },
        weapon: [],
        tool: [],
        gear: [],
        extras: [[]],
    },
    Felvaxian: {
        food: {
            diet: Diet.Carnivore,
            hungryboi: false,
        },
        weapon: [],
        tool: [],
        gear: [],
        extras: [[]],
    },
    Flea: {
        food: {
            diet: Diet.None,
            hungryboi: false,
        },
        weapon: [],
        tool: [],
        gear: [],
        extras: [[]],
    },
    Frog: {
        food: {
            diet: Diet.Default,
            hungryboi: false,
        },
        weapon: [],
        tool: [],
        gear: [],
        extras: [[]],
    },
    Gaia: {
        food: {
            diet: Diet.Vegetarian,
            hungryboi: false,
        },
        weapon: [],
        tool: [],
        gear: [],
        extras: [[]],
    },
    Ghast: {
        food: {
            diet: Diet.Default,
            hungryboi: false,
        },
        weapon: [],
        tool: [],
        gear: [],
        extras: [[]],
    },
    Giant: {
        food: {
            diet: Diet.Default,
            hungryboi: true,
        },
        weapon: [],
        tool: [],
        gear: [],
        extras: [[]],
    },
    Glacier: {
        food: {
            diet: Diet.Default,
            hungryboi: false,
        },
        weapon: [],
        tool: [],
        gear: [],
        extras: [[]],
    },
    Gnoll: {
        food: {
            diet: Diet.Carnivore,
            hungryboi: false,
        },
        weapon: [],
        tool: [],
        gear: [],
        extras: [[]],
    },
    "Golden Golem": {
        food: {
            diet: Diet.Default,
            hungryboi: false,
        },
        weapon: [],
        tool: [],
        gear: [],
        extras: [[]],
    },
    Goolien: {
        food: {
            diet: Diet.Default,
            hungryboi: false,
        },
        weapon: [],
        tool: [],
        gear: [],
        extras: [[]],
    },
    "Half-Wither": {
        food: {
            diet: Diet.Default,
            hungryboi: true,
        },
        weapon: [],
        tool: [],
        gear: [],
        extras: [[]],
    },
    Hellforged: {
        food: {
            diet: Diet.Default,
            hungryboi: false,
        },
        weapon: [],
        tool: [],
        gear: [],
        extras: [[]],
    },
    "Hero of The Wild": {
        food: {
            diet: Diet.Default,
            hungryboi: true,
        },
        weapon: [],
        tool: [],
        gear: [],
        extras: [[]],
    },
    Human: {
        food: {
            diet: Diet.Default,
            hungryboi: false,
        },
        weapon: [],
        tool: [],
        gear: [],
        extras: [[]],
    },
    "Ice King": {
        food: {
            diet: Diet.Default,
            hungryboi: false,
        },
        weapon: [],
        tool: [],
        gear: [],
        extras: [[]],
    },
    "Ice Porcupine": {
        food: {
            diet: Diet.Carnivore,
            hungryboi: false,
        },
        weapon: [],
        tool: [],
        gear: [],
        extras: [[]],
    },
    Iceling: {
        food: {
            diet: Diet.Default,
            hungryboi: false,
        },
        weapon: [],
        tool: [],
        gear: [],
        extras: [[]],
    },
    Ignisian: {
        food: {
            diet: Diet.Default,
            hungryboi: false,
        },
        weapon: [],
        tool: [],
        gear: [],
        extras: [[]],
    },
    "Illusioner (Novice)": {
        food: {
            diet: Diet.Default,
            hungryboi: false,
        },
        weapon: [],
        tool: [],
        gear: [],
        extras: [[]],
    },
    Insect: {
        food: {
            diet: Diet.Default,
            hungryboi: false,
        },
        weapon: [],
        tool: [],
        gear: [],
        extras: [[]],
    },
    Jellyfish: {
        food: {
            diet: Diet.Default,
            hungryboi: false,
        },
        weapon: [],
        tool: [],
        gear: [],
        extras: [[]],
    },
    Kelperet: {
        food: {
            diet: Diet.Default,
            hungryboi: false,
        },
        weapon: [],
        tool: [],
        gear: [],
        extras: [[]],
    },
    Kirin: {
        food: {
            diet: Diet.Carnivore,
            hungryboi: true,
        },
        weapon: [],
        tool: [],
        gear: [],
        extras: [[]],
    },
    "Land Shark": {
        food: {
            diet: Diet.Carnivore,
            hungryboi: false,
        },
        weapon: [],
        tool: [],
        gear: [],
        extras: [[]],
    },
    "Light Mage": {
        food: {
            diet: Diet.Default,
            hungryboi: false,
        },
        weapon: [],
        tool: [],
        gear: [],
        extras: [[]],
    },
    "Lost Draconian": {
        food: {
            diet: Diet.Default,
            hungryboi: false,
        },
        weapon: [],
        tool: [],
        gear: [],
        extras: [[]],
    },
    "Lunar Path": {
        food: {
            diet: Diet.Default,
            hungryboi: false,
        },
        weapon: [],
        tool: [],
        gear: [],
        extras: [[]],
    },
    "Magical Manipulator": {
        food: {
            diet: Diet.Default,
            hungryboi: false,
        },
        weapon: [],
        tool: [],
        gear: [],
        extras: [[]],
    },
    Magmean: {
        food: {
            diet: Diet.Custom("minecraft:honey_bottle", 16),
            hungryboi: false,
        },
        weapon: [],
        tool: [],
        gear: [],
        extras: [[]],
    },
    Malfunction: {
        food: {
            diet: Diet.Default,
            hungryboi: false,
        },
        weapon: [],
        tool: [],
        gear: [],
        extras: [[]],
    },
    Marshmallow: {
        food: {
            diet: Diet.SugarAddict,
            hungryboi: true,
        },
        weapon: [],
        tool: [],
        gear: [],
        extras: [[]],
    },
    Merling: {
        food: {
            diet: Diet.Default,
            hungryboi: false,
        },
        weapon: [],
        tool: [],
        gear: [],
        extras: [[]],
    },
    Moth: {
        food: {
            diet: Diet.Default,
            hungryboi: false,
        },
        weapon: [],
        tool: [],
        gear: [],
        extras: [[]],
    },
    Mothling: {
        food: {
            diet: Diet.Custom("minecraft:leather", 32),
            hungryboi: false,
        },
        weapon: [],
        tool: [],
        gear: [],
        extras: ["32x minecraft:rabbit_hide"],
    },
    "Mountain Goat": {
        food: {
            diet: Diet.Vegetarian,
            hungryboi: false,
        },
        weapon: [],
        tool: [],
        gear: [],
        extras: [[]],
    },
    Mouse: {
        food: {
            diet: Diet.Default,
            hungryboi: false,
        },
        weapon: [],
        tool: [],
        gear: [],
        extras: [[]],
    },
    Ninetails: {
        food: {
            diet: Diet.Default,
            hungryboi: false,
        },
        weapon: [],
        tool: [],
        gear: [],
        extras: [[]],
    },
    Panoptican: {
        food: {
            diet: Diet.Default,
            hungryboi: false,
        },
        weapon: [],
        tool: [],
        gear: [],
        extras: [[]],
    },
    "Part-Robot": {
        food: {
            diet: Diet.None,
            hungryboi: false,
        },
        weapon: [],
        tool: [],
        gear: [],
        extras: [[]],
    },
    Phantom: {
        food: {
            diet: Diet.Default,
            hungryboi: true,
        },
        weapon: [],
        tool: [],
        gear: [],
        extras: [[]],
    },
    Pixie: {
        food: {
            diet: Diet.Vegetarian,
            hungryboi: false,
        },
        weapon: [],
        tool: [],
        gear: [],
        extras: [[]],
    },
    Ram: {
        food: {
            diet: Diet.Custom("minecraft:wheat", 64),
            hungryboi: false,
        },
        weapon: [],
        tool: [],
        gear: [],
        extras: [[]],
    },
    Raptus: {
        food: {
            diet: Diet.Carnivore,
            hungryboi: false,
        },
        weapon: [],
        tool: [],
        gear: [],
        extras: [[]],
    },
    Rat: {
        food: {
            diet: Diet.Default,
            hungryboi: false,
        },
        weapon: [],
        tool: [],
        gear: [],
        extras: [
            Item.of(
                "supplementaries:present_pink",
                1,
                '{BlockEntityTag:{Description:"Enjoy the lil seat for the ratski to relaxski on",Items:[{Count:1b,Slot:0b,id:"create:pink_seat"}],Recipient:"The Rat",Sender:"Stapleton_"}}'
            ),
        ],
    },
    Ravager: {
        food: {
            diet: Diet.Default,
            hungryboi: true,
        },
        weapon: [],
        tool: [],
        gear: [],
        extras: [[]],
    },
    "Red Panda": {
        food: {
            diet: Diet.Custom("minecraft:bamboo", 32),
            hungryboi: false,
        },
        weapon: [],
        tool: [],
        gear: [],
        extras: [[]],
    },
    "Reign Farmer": {
        food: {
            diet: Diet.Vegetarian,
            hungryboi: false,
        },
        weapon: [],
        tool: [],
        gear: [],
        extras: [[]],
    },
    "Rift Mage": {
        food: {
            diet: Diet.Custom("minecraft:ender_pearl", 32),
            hungryboi: false,
        },
        weapon: [],
        tool: [],
        gear: [],
        extras: [[]],
    },
    Ryū: {
        food: {
            diet: Diet.Carnivore,
            hungryboi: true,
        },
        weapon: [],
        tool: [],
        gear: [],
        extras: [[]],
    },
    "Sand-Walker": {
        food: {
            diet: Diet.Custom("minecraft:rotten_flesh", 32),
            hungryboi: false,
        },
        weapon: [],
        tool: [],
        gear: [],
        extras: [[]],
    },
    Shadow: {
        food: {
            diet: Diet.None,
            hungryboi: false,
        },
        weapon: [],
        tool: [],
        gear: [],
        extras: [[]],
    },
    "Shadow Crawler": {
        food: {
            diet: Diet.None,
            hungryboi: false,
        },
        weapon: [],
        tool: [],
        gear: [],
        extras: [[]],
    },
    Shifter: {
        food: {
            diet: Diet.Custom("minecraft:honey_bottle", 32),
            hungryboi: false,
        },
        weapon: [],
        tool: [],
        gear: [],
        extras: [[]],
    },
    Shulk: {
        food: {
            diet: Diet.Default,
            hungryboi: true,
        },
        weapon: [],
        tool: [],
        gear: [],
        extras: [[]],
    },
    Snake: {
        food: {
            diet: Diet.Default,
            hungryboi: false,
        },
        weapon: [],
        tool: [],
        gear: [],
        extras: [[]],
    },
    "Soul-Seer": {
        food: {
            diet: Diet.None,
            hungryboi: false,
        },
        weapon: [],
        tool: [],
        gear: [],
        extras: [[]],
    },
    Spectre: {
        food: {
            diet: Diet.Custom("minecraft:rotten_flesh", 32),
            hungryboi: false,
        },
        weapon: [],
        tool: [],
        gear: [],
        extras: [[]],
    },
    "Spirit Ram": {
        food: {
            diet: Diet.Default,
            hungryboi: false,
        },
        weapon: [],
        tool: [],
        gear: [],
        extras: [[]],
    },
    Sporeling: {
        food: {
            diet: Diet.Default,
            hungryboi: false,
        },
        weapon: [],
        tool: [],
        gear: [],
        extras: [[]],
    },
    Sprinter: {
        food: {
            diet: Diet.Carnivore,
            hungryboi: false,
        },
        weapon: [],
        tool: [],
        gear: [],
        extras: [[]],
    },
    Stargazer: {
        food: {
            diet: Diet.Default,
            hungryboi: false,
        },
        weapon: [],
        tool: [],
        gear: [],
        extras: [[]],
    },
    "Sunker Sailor": {
        food: {
            diet: Diet.Default,
            hungryboi: false,
        },
        weapon: [],
        tool: [],
        gear: [],
        extras: [[]],
    },
    Technomancer: {
        food: {
            diet: Diet.Default,
            hungryboi: false,
        },
        weapon: [],
        tool: [],
        gear: [],
        extras: [[]],
    },
    Thornling: {
        food: {
            diet: Diet.Default,
            hungryboi: false,
        },
        weapon: [],
        tool: [],
        gear: [],
        extras: [[]],
    },
    Triangulon: {
        food: {
            diet: Diet.Default,
            hungryboi: false,
        },
        weapon: [],
        tool: [],
        gear: [],
        extras: [[]],
    },
    Vishaichian: {
        food: {
            diet: Diet.Raw,
            hungryboi: false,
        },
        weapon: [],
        tool: [],
        gear: [],
        extras: [[]],
    },
    "Void Samurai": {
        food: {
            diet: Diet.None,
            hungryboi: false,
        },
        weapon: [],
        tool: [],
        gear: [],
        extras: [[]],
    },
    Voidling: {
        food: {
            diet: Diet.Default,
            hungryboi: false,
        },
        weapon: [],
        tool: [],
        gear: [],
        extras: [[]],
    },
    "Volcanic Dragon": {
        food: {
            diet: Diet.Carnivore,
            hungryboi: true,
        },
        weapon: [],
        tool: [],
        gear: [],
        extras: [[]],
    },
    "Wailing One": {
        food: {
            diet: Diet.Default,
            hungryboi: false,
        },
        weapon: [],
        tool: [],
        gear: [],
        extras: [[]],
    },
    Wanderer: {
        food: {
            diet: Diet.Default,
            hungryboi: false,
        },
        weapon: [],
        tool: [],
        gear: [],
        extras: [[]],
    },
    "Wandering Spirit": {
        food: {
            diet: Diet.Default,
            hungryboi: false,
        },
        weapon: [],
        tool: [],
        gear: [],
        extras: [[]],
    },
    "War God": {
        food: {
            diet: Diet.Default,
            hungryboi: true,
        },
        weapon: [],
        tool: [],
        gear: [],
        extras: [[]],
    },
    Warden: {
        food: {
            diet: Diet.Default,
            hungryboi: false,
        },
        weapon: [],
        tool: [],
        gear: [],
        extras: [[]],
    },
    Warforged: {
        food: {
            diet: Diet.Default,
            hungryboi: false,
        },
        weapon: [],
        tool: [],
        gear: [],
        extras: [[]],
    },
    Warper: {
        food: {
            diet: Diet.Custom("minecraft:end_stone", 32),
            hungryboi: false,
        },
        weapon: [],
        tool: [],
        gear: [],
        extras: [[]],
    },
    "Witch of Ink": {
        food: {
            diet: Diet.None,
            hungryboi: false,
        },
        weapon: [],
        tool: [],
        gear: [],
        extras: ["8x minecraft:painting"],
    },
    "Withered Fox": {
        food: {
            diet: Diet.Custom("minecraft:sweet_berries", 32),
            hungryboi: false,
        },
        weapon: [],
        tool: [],
        gear: [],
        extras: [[]],
    },
    "Withered Skeletian": {
        food: {
            diet: Diet.Default,
            hungryboi: false,
        },
        weapon: [],
        tool: [],
        gear: [],
        extras: [[]],
    },
    Wizard: {
        food: {
            diet: Diet.Default,
            hungryboi: false,
        },
        weapon: [],
        tool: [],
        gear: [],
        extras: [[]],
    },
    Wyverian: {
        food: {
            diet: Diet.Carnivore,
            hungryboi: false,
        },
        weapon: [],
        tool: [],
        gear: [],
        extras: [[]],
    },
    "Zero Aizawa": {
        food: {
            diet: Diet.Default,
            hungryboi: false,
        },
        weapon: [],
        tool: [],
        gear: [],
        extras: [[]],
    },
};

function GetFood(Package) {
    let [count, item] = Package.food.diet.split("x ");
    if (Package.food.hungryboi) {
        count = count * 2;
    }
    return `${count}x ${item}`;
}

//origins-classes:class, origins:origin
ServerEvents.customCommand("CarePackage", (event) => {
    let Comp = ModComponents.ORIGIN.get(event.getPlayer());
    let Layer = OriginLayers().getLayer("origins:origin");
    let Origin = Comp.getOrigin(Layer).getName().getString();
    let OriginPackage = Packages[Origin];
    let GameStageID = `CarePackage.${Origin.replace(" ", "_")}`;

    if (event.getPlayer().stages.has(GameStageID)) {
        event.server.runCommandSilent("say You have already collected this Care Package.");
        return;
    }

    event.getPlayer().stages.add(GameStageID);

    Packages.CommonItems.forEach((item) => {
        event.getPlayer().give(item);
    });

    event.getPlayer().give(GetFood(OriginPackage));

    OriginPackage.weapon.forEach((item) => {
        event.getPlayer().give(item);
    });
    OriginPackage.tool.forEach((item) => {
        event.getPlayer().give(item);
    });
    OriginPackage.gear.forEach((item) => {
        event.getPlayer().give(item);
    });
    OriginPackage.extras.forEach((item) => {
        event.getPlayer().give(item);
    });
});
