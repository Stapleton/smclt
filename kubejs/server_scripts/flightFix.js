const Pal = Java.loadClass("io.github.ladysnake.pal.Pal");
const VanillaAbilities = Java.loadClass("io.github.ladysnake.pal.VanillaAbilities");

const FLIGHT_CHARM = Pal.getAbilitySource("ring_of_flight");

function FixFlight(event) {
    if (FLIGHT_CHARM.grants(event.getEntity(), VanillaAbilities.ALLOW_FLYING)) {
        FLIGHT_CHARM.revokeFrom(event.getEntity(), VanillaAbilities.ALLOW_FLYING);
    } else {
        FLIGHT_CHARM.grantTo(event.getEntity(), VanillaAbilities.ALLOW_FLYING);
    }
}

PlayerEvents.loggedIn((event) => {
    FixFlight(event);
});

ServerEvents.customCommand("flightFix", (event) => {
    FixFlight(event);
});

CommonAddedEvents.playerRespawn((event) => {
    FixFlight(event);
});

CommonAddedEvents.playerChangeDimension((event) => {
    FixFlight(event);
});
