const AdditionalSeeds = [
    "croptopia:artichoke_seed",
    "croptopia:asparagus_seed",
    "croptopia:barley_seed",
    "croptopia:basil_seed",
    "croptopia:bellpepper_seed",
    "croptopia:blackbean_seed",
    "croptopia:blackberry_seed",
    "croptopia:blueberry_seed",
    "croptopia:broccoli_seed",
    "croptopia:cabbage_seed",
    "croptopia:cantaloupe_seed",
    "croptopia:cauliflower_seed",
    "croptopia:celery_seed",
    "croptopia:chile_pepper_seed",
    "croptopia:coffee_seed",
    "croptopia:corn_seed",
    "croptopia:cranberry_seed",
    "croptopia:cucumber_seed",
    "croptopia:currant_seed",
    "croptopia:eggplant_seed",
    "croptopia:elderberry_seed",
    "croptopia:garlic_seed",
    "croptopia:ginger_seed",
    "croptopia:grape_seed",
    "croptopia:greenbean_seed",
    "croptopia:greenonion_seed",
    "croptopia:honeydew_seed",
    "croptopia:hops_seed",
    "croptopia:kale_seed",
    "croptopia:kiwi_seed",
    "croptopia:leek_seed",
    "croptopia:lettuce_seed",
    "croptopia:mustard_seed",
    "croptopia:oat_seed",
    "croptopia:olive_seed",
    "croptopia:onion_seed",
    "croptopia:peanut_seed",
    "croptopia:pepper_seed",
    "croptopia:pineapple_seed",
    "croptopia:radish_seed",
    "croptopia:raspberry_seed",
    "croptopia:rhubarb_seed",
    "croptopia:rice_seed",
    "croptopia:rutabaga_seed",
    "croptopia:saguaro_seed",
    "croptopia:soybean_seed",
    "croptopia:spinach_seed",
    "croptopia:squash_seed",
    "croptopia:strawberry_seed",
    "croptopia:sweetpotato_seed",
    "croptopia:tea_seed",
    "croptopia:tomatillo_seed",
    "croptopia:tomato_seed",
    "croptopia:turmeric_seed",
    "croptopia:turnip_seed",
    "croptopia:vanilla_seeds",
    "croptopia:yam_seed",
    "croptopia:zucchini_seed",
    "farmersdelight:cabbage_seeds",
    "farmersdelight:cabbage_seeds",
    "farmersdelight:tomato_seeds",
    "spawn:sunflower_seeds",
    "supplementaries:flax_seeds",
];

LootJS.modifiers((event) => {
    let loot = [];
    event.addBlockLootModifier("minecraft:grass").pool((pool) => {
        pool.rolls([0, 1]);
        pool.randomChance(0.2);
        for (let index in AdditionalSeeds) {
            let Seed = AdditionalSeeds[index];
            loot.push(Item.of(Seed).withChance(1));
        }
        pool.addWeightedLoot([0, 1], loot);
    });
    /*for (let index in AdditionalSeeds) {
        let Seed = AdditionalSeeds[index];
        loot.push(Item.of(Seed).withChance(1));
    }
    event.addBlockLootModifier("minecraft:grass").addWeightedLoot([0, 1], loot);*/
});
