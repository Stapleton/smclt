# Client Install
### Launcher Method
You must have Prism Launcher or MultiMC installed and your MC account connected.
You must also have Git for Windows installed. Just click next all the way through.

Download the zip file SMC Long Term V∞.zip. Open Prism or MultiMC. Hit 'Add Instance' in the top left corner. Select Import on the left side. Browse for the export zip file you downloaded. Hit OK in the bottom right of the window. Launch the newly added instance!

Benefits of a Prism/MultiMC specific export:
- Smaller pack export file
- Pre-launch commands for running the launch scripts I wrote for the pack

Benefits of my launch script due to Prism/MultiMC
- Automatically installs all modpack resources on first launch thanks to pre-launch commands
- Automatically updates all modpack resources on every subsequent launch thanks to pre-launch commands
- Respects user option changes by being able to ignore those specific files during updates from the GitLab

### Scripted Install
Download the zip file SMC Long Term V∞.zip. Open the zip file, take out the mmc-pack.json file and open it in your favoured text editor. Please note down the minecraft version and fabric version. (cachedVersion or version)

Create a new instance in whatever launcher you favour according to those versions.
Once thats complete, download the LaunchClient script for whichever OS you're on. (.sh - Linux/Unix, .bat - Windows)
Put the script in the instance folder on the same level as the mods/ folder.
Run the script, and itll install for you. This script also updates the pack from Git.
**HIGHLY RECOMMENDED - For a smooth experience** 
Please figure out a way to launch the script automatically before the pack.
I will not provide you solutions as everyones setup and available tools are different but if you're already using this guide, you already have the self-belief to figure things out for yourself, and I believe you can figure out a script to run the pack script alongside launching the pack from your launcher via command line. Hint, its probably only two lines.

# Server Install
### No Launcher. Script Only.
Download the launchServer script for whatever OS you're on. (.sh - Linux/Unix, .bat - Windows)
Run the script! It'll install the server in its entirety for you and run it for you.
Every subsequent run of the script will autoupdate the server from Git and proceed to launch it for you.